#!/usr/bin/env bash

# This script clones or updates the kernel-watch-repo used by
# the subsystems webhook.

set -e

if [[ -d ${LOCAL_REPO_PATH} ]]; then
	cd ${LOCAL_REPO_PATH}
	git pull
else
	mkdir -p ${LOCAL_REPO_PATH}
	git clone ${KERNEL_WATCH_URL} ${LOCAL_REPO_PATH}
fi
